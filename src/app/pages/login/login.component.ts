import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import Swal from 'sweetalert2';
import { LoginService } from '../../shared/services/login.service';
import { loginresponse } from '../../shared/urls/responses';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin = new FormGroup({
    user: new FormControl('', [Validators.required]),
    pass: new FormControl('', [Validators.required, Validators.minLength(6)])
  })

  private jwtHelper:JwtHelperService = new JwtHelperService;

  constructor(
    private loginService: LoginService,
    private router:Router
    ) { }

  ngOnInit(): void {
    if(this.loginService.islogged()){
      this.router.navigate(['home']);
    }
  }

  async login(){
    if(this.formLogin.valid){
      this.formLogin.disable();
      this.formLogin.value;
      this.loginService.signIn(this.formLogin.value).toPromise().then((resp:HttpResponse<loginresponse|any>)=>{
        console.log(resp);
        console.log(resp.body.data.payload);
        console.log(this.jwtHelper.decodeToken(resp.body.data.payload.token));
        localStorage.setItem("token",resp.body.data.payload.token);
        localStorage.setItem("refresh_token",resp.body.data.payload.refresh_token);
        localStorage.setItem("name",resp.body.data.user.firstName);
        localStorage.setItem("lastname",resp.body.data.user.lastName);
        if(this.loginService.islogged()){
          this.router.navigate(['home']);
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'autenticacion fallida',
            heightAuto:false
          })
        }
      }).catch(err=>{console.log(err);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'hubo un error al autenticar verifique sus credenciales',
          heightAuto:false
        })
        this.formLogin.enable();
      })
    }
    else{
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'datos invalidos',
        heightAuto:false
      })
    }
  }

}
