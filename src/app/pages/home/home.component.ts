import { Component, OnInit, Output } from '@angular/core';
import { HomeService } from '../../shared/services/home.service';
import { HttpResponse } from '@angular/common/http';
import { popularresponse, respmovie, loginresponse } from '../../shared/urls/responses';
import { Router } from '@angular/router';
import { LoginService } from '../../shared/services/login.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private page:number=0;
  public imagebase:String="";
  public imagepremierebase:String="";
  public movies:Array<respmovie>=[];
  public premieremovies:Array<respmovie>=[];

  constructor(private homeService:HomeService,
    private router:Router,
    private loginService:LoginService
  ) { }

  ngOnInit(): void {
    this.addMovies();
    this.addPremiereMovies();
  }

  async addMovies(){
    this.page++;
    console.log(this.page);
    if(!this.loginService.islogged()){
      await this.relogin();
    }
    this.homeService.getPopular(this.page).toPromise().then((resp:HttpResponse<popularresponse|any>)=>{
      this.imagebase=resp.body.imageBaseUrl;
      resp.body.data.forEach((movie:respmovie) => {
        this.movies.push(movie);
      });
    })
    
  }
  async addPremiereMovies(){
    this.page++;
    if(!this.loginService.islogged()){
      await this.relogin();
    }
    console.log(this.page);
    this.homeService.getPremiere(this.page).toPromise().then((resp:HttpResponse<popularresponse|any>)=>{
      this.imagepremierebase=resp.body.imageBaseUrl;
      resp.body.data.forEach((movie:respmovie) => {
        this.premieremovies.push(movie);
      });
    })
    
  }

  onScroll(){
    this.addMovies();
  }
  onScrollH(){
    this.addPremiereMovies();
  }

  goToPage(movie:respmovie){
    let data={
      img:`${this.imagebase}${movie.poster_path}`,
      background:`${this.imagebase}${movie.backdrop_path}`,
      overview:movie.overview,
      id:movie.id,
      original_title:movie.original_title,
      title:movie.title
    }
    localStorage.setItem("selectedmovie",JSON.stringify(data));
    this.router.navigate(["detail"]);
  }

 /*  onWheel(event: WheelEvent): void {
    (el:HTMLElement).parentElement.scrollLeft += event.deltaY;
    event.preventDefault();
 }  */

  relogin(){
    this.loginService.refresh().toPromise().then((resp:HttpResponse<loginresponse|any>)=>{
      console.log("refresh");
      localStorage.setItem("token",resp.body.data.payload.token);
      localStorage.setItem("refresh_token",resp.body.data.payload.refresh_token);
      localStorage.setItem("name",resp.body.data.user.firstName);
      localStorage.setItem("lastname",resp.body.data.user.lastName);
  })}

}
