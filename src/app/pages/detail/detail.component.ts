import { Component, OnInit } from '@angular/core';
import { respmovie, respactors, respactor, loginresponse } from '../../shared/urls/responses';
import { Router } from '@angular/router';
import { DetailService } from '../../shared/services/detail.service';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { LoginService } from '../../shared/services/login.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  public movie:any;
  public actors:Array<respactor>=[];
  public imagebaseactor:String=""

  constructor(
    private router:Router,
    private detailService:DetailService,
    private loginService:LoginService
  ) {
  }

  async ngOnInit(): Promise<void> {
    let storedMovie = localStorage.getItem("selectedmovie");
    if(typeof storedMovie === "string"){
      this.movie = JSON.parse(storedMovie);
      if(!this.loginService.islogged()){
        await this.relogin();
      }
      this.detailService.getActors(this.movie["id"]).toPromise().then((resp:HttpResponse<respactors|any>)=>{
        this.imagebaseactor=resp.body.imageBaseUrl;
        this.actors=resp.body.data;
      });
    }
    else{
      this.router.navigate(["home"])
    }
  }

  goBack(){
    this.router.navigate(["home"])
  }

  relogin(){
    this.loginService.refresh().toPromise().then((resp:HttpResponse<loginresponse|any>)=>{
      console.log("refresh");
      localStorage.setItem("token",resp.body.data.payload.token);
      localStorage.setItem("refresh_token",resp.body.data.payload.refresh_token);
      localStorage.setItem("name",resp.body.data.user.firstName);
      localStorage.setItem("lastname",resp.body.data.user.lastName);
  })}

}
