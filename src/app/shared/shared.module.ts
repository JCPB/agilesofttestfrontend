import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppbarComponent } from './components/appbar/appbar.component';
import { MatCardModule } from '@angular/material/card';
import { AngularMaterialModule } from '../angular-material.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';

const components=[AppbarComponent]

@NgModule({
  declarations: components,
  exports: components,
  imports: [
    CommonModule,
    AngularMaterialModule,
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
