import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Urls } from '../urls/urls';
import { JwtHelperService } from '@auth0/angular-jwt';
import { loginresponse } from '../urls/responses';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private urls:Urls = new Urls
  private jwtHelper:JwtHelperService = new JwtHelperService;
  constructor(
    private http: HttpClient,
    ) { }

  public signIn = (data:{user:String,pass:String}) => {
    console.log(data);
    const raw = JSON.stringify({ username: data['user'], password: data['pass'] });
    let header = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
    return this.http.post(`${this.urls.login.signin}`, raw, { headers: header, observe: 'response'});
  }

  public refresh = () => {
      const raw = JSON.stringify({
        refresh_token: localStorage.getItem("refresh_token")
      });
      let header = new HttpHeaders()
        .set('Content-Type', 'application/json; charset=utf-8')
      return this.http.post(`${this.urls.login.signin}`, raw, { headers: header, observe: 'response'});
  }

  public decodeJWT = ()=>{
    let tkn='token';
    let jwt = localStorage.getItem(tkn);
    if(jwt == null || typeof jwt == undefined){
      return null;
    }
    //return JSON.parse(atob(jwt.split(".")[1]));
    return this.jwtHelper.decodeToken(jwt)
  }

  public islogged = ()=>{
    let now = new Date();
    let valid = false;
    let decoded = this.decodeJWT()
    if(decoded == null){
      return valid;
    }
    if(this.decodeJWT()["exp"]*1000>now.getTime()){
      valid = true;
    }
    return valid;
  }
}
