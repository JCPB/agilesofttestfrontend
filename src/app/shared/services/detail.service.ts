import { Injectable } from '@angular/core';
import { Urls } from '../urls/urls';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DetailService {

  private urls: Urls = new Urls();

  constructor(private http:HttpClient) { }

  public getActors(id:String) {
    let header = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set("Authorization", "Bearer " + localStorage.getItem('token'))
    return this.http.get(`${this.urls.home.actors(id)}`, { headers: header, observe: 'response' });
  }
}
