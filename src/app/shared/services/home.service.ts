import { Injectable } from '@angular/core';
import { Urls } from '../urls/urls';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private urls: Urls = new Urls();

  constructor(
    private http:HttpClient
  ) { }

  public getPopular(page=1) {
    let header = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set("Authorization", "Bearer " + localStorage.getItem('token'))
    return this.http.get(`${this.urls.home.popular}?page=${page}`, { headers: header, observe: 'response' });
  }
  public getPremiere(page=1) {
    let header = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set("Authorization", "Bearer " + localStorage.getItem('token'))
    return this.http.get(`${this.urls.home.premiere}?page=${page}`, { headers: header, observe: 'response' });
  }
}
