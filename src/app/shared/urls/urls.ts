export class Urls{
    private baseurl="http://161.35.140.236:9005/api";
    public login={
        signin:this.baseurl+"/auth/login",
        refresh:this.baseurl+"/auth/refresh"
    }
    public home={
        premiere:this.baseurl+"/movies/now_playing",
        popular:this.baseurl+"/movies/now_playing",
        actors:(act:String)=>this.baseurl+"/movies/"+act+"/actors",
    }
}