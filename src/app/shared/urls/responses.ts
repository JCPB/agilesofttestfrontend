export interface loginresponse{
    data:{
        user:{
            email:String,
            firstName:String,
            lastName:String
        }
    }
    payload:{
        type:String,
        token:String,
        refresh_token:String
    }
}
export interface popularresponse{
    imageBaseUrl:String;
    data:[respmovie]
}

export interface respmovie{
    adult:String,
    backdrop_path:String,
    genre_ids:[number],
    id:number,
    original_language:String,
    original_title:String,
    overview:String,
    popularity:number,
    poster_path:String,
    release_date:String,
    title:String,
    video:boolean,
    vote_averate:number,
    vote_count:number
}

export interface respactors{
    imageBaseUrl:String,
    data:[respactor]
}
export interface respactor{
    adult: boolean,
    gender: number,
    id: number,
    known_for_department: String,
    name: String,
    original_name: String,
    popularity: number,
    profile_path: String,
    cast_id: number,
    character: String,
    credit_id: String,
    order:number
}