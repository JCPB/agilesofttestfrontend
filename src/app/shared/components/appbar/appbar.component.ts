import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-appbar',
  templateUrl: './appbar.component.html',
  styleUrls: ['./appbar.component.css']
})
export class AppbarComponent implements OnInit {

  public name:String=localStorage.getItem("name")+" "+localStorage.getItem("lastname");

  constructor(
    private loginService:LoginService,
    private router:Router
  ) { }

  ngOnInit(): void {
  }
  async logout(){
    localStorage.clear();
    if(!this.loginService.islogged()){
      this.router.navigate(['login']);
    }
  }

}
