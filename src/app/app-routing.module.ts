import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailComponent } from './pages/detail/detail.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { LoginActiveGuard } from './shared/guards/login-active.guard';

const routes: Routes = [
  {path:"", redirectTo:"login", pathMatch:"full"},
  {path: 'login',component:LoginComponent},
  {path: 'home',component:HomeComponent, canActivate:[LoginActiveGuard]},
  {path: 'detail',component:DetailComponent,canActivate:[LoginActiveGuard]},
  {path: '**',component:NotfoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
